extends Node

var last_monstercollision_pos
var coll_count

func _ready():
	Global.GameManager = self

func _monsters_collided(position):
	coll_count += 1
	last_monstercollision_pos = position
	if coll_count >= 2:
		coll_count = 0
		get_tree().quit()
