extends AnimatedSprite

var first_frame = 0
var numof_frames
var wavename
signal animation_played

func _ready():
	set_process(false)
	Global.wave_text=self

func play_wave_animation(wave):
	if wave==4:
		print(4)
	wavename = "wave_"+str(wave)
	play(wavename)
	visible = true
	numof_frames = frames.get_frame_count(wavename)-1

func _on_wave_text_frame_changed():
	if frame==numof_frames:
		play(wavename, true)
	elif frame==0:
		visible=false
		emit_signal("animation_played")
