extends TextureRect

func _on_X_button_down():
	texture = load("res://sprt/GUI/Dialog_two_buttons_x.png")


func _on_X_button_up():
	texture = load("res://sprt/GUI/Dialog_two_buttons.png")


func _on_X_pressed():
	yield(get_tree().create_timer(0.14), "timeout")
	get_tree().reload_current_scene()


func _on_next_button_down():
	texture = load("res://sprt/GUI/Dialog_two_buttons_next.png")


func _on_next_button_up():
	texture = load("res://sprt/GUI/Dialog_two_buttons.png")


func _on_next_pressed():
	yield(get_tree().create_timer(0.14), "timeout")
	get_tree().reload_current_scene()
