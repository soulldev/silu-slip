extends AnimatedSprite

signal finished
var first_frame
var last_frame
var wavetext

func _ready():
	first_frame=0
	last_frame=frames.get_frame_count("wave 1")-1
	Global.wave_text = self

func play_wave_animation(wave):
	wavetext = "wave "+str(wave)
	last_frame=frames.get_frame_count("wave 1")-1
	play("wave 2")

func _on_big_wave_text_frame_changed():
	if frame==last_frame:
		play(wavetext, true)
	elif frame==first_frame:
		emit_signal("finished")
