extends Area2D

var direction
var speed = 600

func _ready():
	set_physics_process(false)

func launch():
	set_physics_process(true)

func _physics_process(delta):
	position += direction.normalized() * speed * delta


func _on_driple_body_entered(body):
	body.hit()
	queue_free()
