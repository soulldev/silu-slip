extends Area2D

var health = 4
var speed = 250
var player_position
onready var bullet = load("res://scns/jelly_boss_bullet.tscn")
var hands = ["right", "left"]
var coordinate_to_start_with
var hit_type = 3
onready var slave_summoner = load("res://scns/jellyboss_slave_summon.tscn")
func _ready():
	randomize()
	Global.boss = self
	set_physics_process(false)
	yield(get_tree().create_timer(rand_range(0.5, 0.7)), "timeout")
	fire_bullets()
	$t_move.start()

func _process(delta):
	$health_sprite.frame = health-1

func _on_t_move_timeout():
	player_position = Global.silo.position
	coordinate_to_start_with = randi()%2
	if coordinate_to_start_with == 0:
		move_x(player_position.x, Global.Grid.grids_between(position.x, player_position.x))
		yield($Tween, "tween_completed")
		move_y(player_position.y, Global.Grid.grids_between(position.y, player_position.y))
	else:
		move_y(player_position.y, Global.Grid.grids_between(position.y, player_position.y))
		yield($Tween, "tween_completed")
		move_x(player_position.x, Global.Grid.grids_between(position.x, player_position.x))
	yield($Tween, "tween_completed")
	set_physics_process(true)
	stop_and_hit()

func fire_bullets():
	for i in hands:
		var dir =  Global.silo.position - get_node(i+"_hand").global_position
		var b = bullet.instance()
		b.global_position = get_node(i+"_hand").global_position
		get_parent().add_child(b)
		b.direction = dir
		yield(get_tree().create_timer(0.2), "timeout")
		b.launch()
		yield(get_tree().create_timer(0.5), "timeout")
		Global.PointsSpawner.spawn()

func stop_and_hit():
	if hit_type<2:
		fire_bullets()
		hit_type += 1
	else:
		yield(get_tree().create_timer(0.3), "timeout")
		player_position = Global.silo.position
		spawn_slaves()
		yield(get_tree().create_timer(1.3), "timeout")
		var player_position2 = Global.silo.position
		if (player_position.y != player_position2.y):
			spawn_slaves()
		hit_type = 1
	$t_move.start()

func spawn_slaves():
	#TODO fix, doesn't summon up
	for pos in Global.GridSpawner.all_possible_spawn_positions["LEFT"]:
		if pos.y >= Global.silo.position.y-40 and pos.y <= Global.silo.position.y+40:
			var ss = slave_summoner.instance()
			ss.position = pos
			ss.slave_rotation = Global.GridSpawner.monster_rotations[1]
			ss.slave_direction = Global.GridSpawner.monster_directions[1]
			ss.slave_speed = rand_range(300, 320)
			get_parent().add_child(ss)
	for pos in Global.GridSpawner.all_possible_spawn_positions["RIGHT"]:
		if pos.y >= Global.silo.position.y-40 and pos.y <= Global.silo.position.y+40:
			var ss = slave_summoner.instance()
			ss.position = pos
			ss.slave_rotation = Global.GridSpawner.monster_rotations[2]
			ss.slave_direction = Global.GridSpawner.monster_directions[2]
			ss.slave_speed = rand_range(300, 320)
			get_parent().add_child(ss)

func move_x(x, duration):
	$Tween.interpolate_property(self,"position", null, Vector2(x, position.y), (duration * 1.0)/3.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func move_y(y, duration):
	$Tween.interpolate_property(self,"position", null, Vector2(position.x,y), (duration * 1.0)/3.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_jelly_boss_body_entered(body):
	body.hit()
