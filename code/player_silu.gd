extends KinematicBody2D

signal hit

var charged = "no"
var MAX_SPEED = 470.0
var ACCELARATION = MAX_SPEED/4
var motion = Vector2()
var velocity = Vector2()
var STOP = Vector2(0,0)
var UP = Vector2(0,-1)
var DOWN = Vector2(0,1)
var LEFT = Vector2(-1,0)
var RIGHT = Vector2(1,0)
var driple = load("res://scns/driple.tscn")
var grid
var last_direction = Vector2()
var attacking

func _ready():
	Global.silo = self
	velocity=STOP
	randomize()
	yield(get_tree().create_timer(0.001), "timeout")
	grid = Global.Grid
	
func _physics_process(delta):
	motion.x = float((velocity == RIGHT)) * min((motion.x)+ACCELARATION, MAX_SPEED) + (float((velocity == LEFT)) * max((motion.x)-ACCELARATION, -MAX_SPEED))
	motion.y = float((velocity == DOWN)) * min((motion.y)+ACCELARATION, MAX_SPEED) + float((velocity == UP)) * max((motion.y)-ACCELARATION, -MAX_SPEED)
	motion = move_and_slide(motion)

func go_to_nearest_cell():
	velocity = STOP
	$Tween.stop_all()
	var new_pos = position
	if is_instance_valid(grid) and not grid.has_cell(position):
		new_pos = grid.find_nearest_cell(position)
	move_to(new_pos)

func _process(delta):
	if is_instance_valid(Global.Grid):
		if (Global.Grid.exceeded_x_right(position.x) and can_move):
			position.x=-40
			move_x(Global.Grid.next_x(position.x))
		elif (Global.Grid.exceeded_x_left(position.x) and can_move):
			position.x=440
			move_x(Global.Grid.prev_x(position.x))
		elif (Global.Grid.exceeded_y_down(position.y) and can_move):
			position.y=-40
			move_y(Global.Grid.next_y(position.y))
		elif (Global.Grid.exceeded_y_up(position.y) and can_move):
			position.y=840
			move_y(Global.Grid.prev_y(position.y))
	if Input.is_action_just_pressed("ui_up"):
		rotate_to_attack()

func start(pos):
	position = pos
	show()
	$collision.disabled = false
	can_move = true
	set_collision_layer_bit(0, true)

func stop():
	motion=STOP
	velocity=STOP

func rotate_to_attack():
	can_move=false
	attacking = true
	stop()
	$AnimatedSprite.animation = "attack"
	$AnimatedSprite.flip_h = true
	var dir =Global.boss.position - $attack_position.global_position
	var final_rotation = dir.angle()
	$rotation_tween.interpolate_property(self,"rotation", null, final_rotation, 0.09, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$rotation_tween.start()
	

func hit():
	set_collision_layer_bit(0, false)
	Global.boss_pos = Vector2(-1,-1)
	velocity = STOP
	if is_instance_valid(Global.GridSprite):
		Global.GridSprite.remove()
	$Tween.stop_all()
	yield(get_tree().create_timer(0.4), "timeout")
	for i in range(5):
		$AnimatedSprite.self_modulate = Color(2, 1, 1, 1-(i*0.2))
		yield(get_tree().create_timer(0.02), "timeout")
	hide()
	$collision.disabled = true
	emit_signal("hit")
	$AnimatedSprite.self_modulate = Color(1, 1, 1, 1)
	get_tree().call_group("enemys", "die")
	Global._set_input_method(false, false)

var can_move=true

func move_x(x):
	$Tween.interpolate_property(self,"position", null, Vector2(x, position.y), 0.166, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func move_y(y):
	$Tween.interpolate_property(self,"position", null, Vector2(position.x,y), 0.166, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func move_to(pos):
	$Tween.interpolate_property(self,"position", null, pos, 0.166, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_completed(object, key):
	can_move=true
	yield(get_tree().create_timer(0.01), "timeout")
	if last_direction!=Vector2():
		_on_SlipDetector_slip(last_direction)

func _on_SwipeDetector_swipe(gesture):
	if (can_move):
		if (gesture.y==0 and gesture.x!=0):
			$AnimatedSprite.flip_h = gesture.x>0
		var sd = gesture
		if sd.x > 0:
			motion.y = 0
			velocity = RIGHT
		elif sd.x < 0:
			motion.y = 0
			velocity = LEFT
		elif sd.y < 0:
			motion.x = 0
			velocity = UP
		elif sd.y > 0:
			motion.x = 0
			velocity = DOWN
		elif sd == Vector2():
			velocity = STOP

func _on_Tween_tween_started(object, key):
	can_move=false


func _on_SlipDetector_slip(gesture):
	if (can_move):
		if (gesture.y==0 and gesture.x!=0):
			$AnimatedSprite.flip_h = gesture.x>0
		var sd = gesture
		var grid = Global.Grid
		if sd.x > 0:
			move_x(grid.next_x(position.x))
		elif sd.x < 0:
			move_x(grid.prev_x(position.x))
		elif sd.y < 0:
			move_y(grid.prev_y(position.y))
		elif sd.y > 0:
			move_y(grid.next_y(position.y))
	last_direction = gesture
	

func rotation2_completed(object, key):
	$AnimatedSprite.animation = "walk"
	can_move=true
	attacking = false


func atk(object, key):
	yield(get_tree().create_timer(0.06), "timeout")
	if Global.boss_pos == Vector2(-1,-1):
		return
	var dir = Global.boss.position - $attack_position.global_position
	var d = driple.instance()
	Global.add_child(d)
	d.global_position = $attack_position.global_position
	d.direction = dir
	d.launch()
	yield(get_tree().create_timer(0.05), "timeout")
	$rotation_tween2.interpolate_property(self,"rotation", null, 0, 0.08, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$rotation_tween2.start()
