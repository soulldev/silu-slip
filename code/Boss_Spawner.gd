extends Node

export (PackedScene) var jelly_boss
export (PackedScene) var bat_boss
var monster_boss
export (PackedScene) var boss_spawn_animation

func spawn_boss():
	if Global.GameManager.current_stage == "jellyfishes":
		monster_boss = jelly_boss
	elif Global.GameManager.current_stage == "dark bats":
		monster_boss = bat_boss
	var boss = monster_boss.instance()
	boss.position = Vector2(200,120)
	Global.main.add_child(boss)
