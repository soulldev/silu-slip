extends AnimatedSprite

onready var boss_slave = load("res://scns/jelly_black.tscn")
var slave_rotation
var slave_direction
var slave_speed

func _ready():
	playing = true

func _on_boss_summon_animation_animation_finished():
	var bs = boss_slave.instance()
	get_parent().add_child(bs)
	bs.position=position
	bs.rotation=slave_rotation
	bs.velocity=slave_direction
	bs.speed=slave_speed
	bs.launch()
	queue_free()
