extends Node

#Known Bugs:
#Explotions Fire create by themselve when not charged , maybe there's a solution
#let the shield has a collision shape for itself
#GUI bug when press try again may hit a monster and die..

export (PackedScene) var spawner
var SlipDetectorScene = load("res://scns/slip_detector.tscn")
var SwipeDetectorScene = load("res://scns/swipe_detector.tscn")
const DefaultManager = preload("res://code/NormalGameManager.gd")
onready var default_manager = DefaultManager.new()

func _ready():
	Global.main = self
	default_manager._ready()
	randomize()
	start()

func add_swipe_detector():
	var swipe_detector = SwipeDetectorScene.instance()
	add_child(swipe_detector)

func add_slip_detector():
	var slip_detector = SlipDetectorScene.instance()
	add_child(slip_detector)

func hitten():
	get_tree().reload_current_scene()

func start():
	Global.silo.start(Global.Grid.middle_position())
	get_tree().call_group("black_holes", "close")
	add_spawner()

func add_spawner():
	var sp = spawner.instance()
	add_child(sp)

func _on_StartTimer_timeout():
	print("timer_started")

#TODO Make the monster spawn, speed, player speed better and alot more calming, think deeply about it
