extends Node

export (PackedScene) var monster

export var mon_minspeed = 180
export var mon_maxspeed = 230
var last_position = Vector2()

func _ready():
	Global.chaos_spawner = self
	randomize()
	set_process(false)

func start_chaos():
	$t_monspawn.start()
	$t_endwave.start()
	get_parent().wave += 1

func _on_t_monspawn_timeout():
	#Generate Position
	get_node("monsterPath/monsterSpawnLocation").offset = randi()
	#Spawn
	var spawn_type = randi()%10
#	if spawn_type < 5:
	spawn_follow()
#	else:
#		spawn_random()

func _on_t_endwave_timeout():
	set_process(true)
	$t_monspawn.stop()
	increase_difficulty()
	set_process(false)
	Global.can_end_wave=true
	
func spawn_follow():
	var mon = monster.instance()
	add_child(mon)
	mon.position = get_node("monsterPath/monsterSpawnLocation").position
	mon.rotation = ((Global.silo.position - mon.position).normalized()).angle()
	mon.velocity = (Global.silo.position - mon.position).normalized()
	mon.speed = rand_range(mon_minspeed-30, mon_maxspeed-38)
	mon.set_physics_process(true)
	
	if (mon.position.x <= last_position.x + 80 and mon.position.x >= last_position.x - 80) or (mon.position.y <= last_position.y + 80 and mon.position.y >= last_position.y - 80):
			if mon.position.x >= 440:
				mon.position.y += rand_range(90,40)
			elif mon.position.x <= -40:
				mon.position.y += rand_range(-90,-40)
			elif mon.position.y <= -40:
				mon.position.x += rand_range(-90,-40)
			elif mon.position.y >= 830:
				mon.position.x += rand_range(90,40)
	
	last_position = mon.position

func increase_difficulty():
	if mon_minspeed < 230:
		mon_minspeed += 10
	if mon_maxspeed < 260:
		mon_maxspeed += 10
	if $t_monspawn.wait_time > 5.5:
		$t_monspawn.wait_time -= 0.25
