extends AnimatedSprite

var numof_default_frames = 9
var first_frame = 0

func _ready():
	animation = "create"

func create():
	visible = true
	play("create")
	$Area2D/CollisionShape2D.disabled = false

func remove():
	$Area2D/CollisionShape2D.disabled = true
	visible = true
	play("remove")

func _on_Shield_animation_finished():
	if animation == "create":
		play("default")
	
	elif animation == "remove":
		playing = false
		$Area2D/CollisionShape2D.disabled = true
		frame = 0
		visible = false
