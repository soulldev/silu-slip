extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _physics_process(delta):
	position.y -=120 * delta

func _ready():
	for i in range(10):
		Global.scr_panel = self
		$Sprite.self_modulate = Color(1,1,1,i*0.1)
		$Label.self_modulate = Color(1,1,1,i*0.1)
		yield(get_tree().create_timer(0.04), "timeout")
	queue_free()
