extends Sprite

var fade = true
const fade_frames= 1
var fade_curr_frame = 0
var fast_fade = false
signal faded

func _ready():
	modulate = Color(1,1,1, 0)
	set_process(false)

func _process(delta):
	if fade:
		if fade_curr_frame < fade_frames:
			if not fast_fade:
				fade_curr_frame += 0.03
			else:
				fade_curr_frame += 0.2
			modulate = Color(1,1,1, fade_curr_frame)
		else:
			fade_curr_frame = 0
			fade = false
			emit_signal("faded")
			set_process(false)

func fade_in():
	modulate = Color(1,1,1, 0)
	fade_curr_frame = 0
	fade = true
	set_process(true)
