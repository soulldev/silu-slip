extends Node


#Everywhere
var coins = 0
var high_score = 0
var GameManager
var GUI
#eeded In_GAME
var wave
var boss_pos = Vector2(-1,-1)
var boss_width = 0
var boss_height = 0
var background
var wave_text
var GridSpawner
var main
var Spawner
var mon_select = false
var ghud
var silo
var mon
var eo
var energy_orb_area
var in_game = false
var scr_panel
var Grid
var GridSprite
var AllMonSpeed
var SlipDetector
var SwipeDetector
var mon_num = 0
var WaveText
var big_wave_text
var boss
var PointsSpawner
var mon_collidable=false
var can_end_wave = true
var current_wave_type
var chaos_spawner

func _set_input_method(swipe, slip):
	Global.silo.get_node("Tween").stop_all()
	silo.velocity=silo.STOP
	get_tree().call_group("input_detectors", "queue_free")
	if swipe:
		main.add_swipe_detector()
	elif slip:
		main.add_slip_detector()
	silo.go_to_nearest_cell()
