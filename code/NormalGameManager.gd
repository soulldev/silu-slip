extends Node

var waves = 0
var boss_waves = 0
var current_stage
var stage_index = -1
var stages = ["jellyfishes", "forestbirds", "ancient spiders", "sea fishes", "dark bats", "dwemer cave", "snakes level", "crab level"]

func _ready():
	Global.GameManager = self

func next_stage():
	stage_index += 5
	current_stage = stages[stage_index]
	

func update_counters():
	if Global.current_wave_type == "grid wave":
		waves += 1
		Global.main.get_node("GUI/wavecounter").update()
	else:
		boss_waves += 1
		Global.main.get_node("GUI/bosswavecounter").update()
