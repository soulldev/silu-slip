extends Area2D

var width = 64 * 3
var height = 30 * 3
var health = 4
var speed = 250
var player_position
onready var blood_hit = load("res://scns/bloodhit_claws.tscn")
var hit_type = 3
onready var bat_slave = load("res://scns/batboss_slave.tscn")
onready var warning = load("res://scns/Warning_220.tscn")
var slave_rotation
var slave_direction
var slave_speed
var bat_slaves = []
var is_hitable = false
var hit3 = true
var hit_times = 0
var fadeout = false
var fadein = false
var last_fadeout_frame=0
var last_fadein_frame=1
var curr_frame = 1
signal faded_out
signal faded_in

func _process(delta):
	$health_sprite.frame = health-1
	if fadeout:
		if curr_frame > last_fadeout_frame:
			curr_frame -= 0.05
			modulate = Color(1,1,1, curr_frame)
		else:
			emit_signal("faded_out")
			curr_frame = 0
			fadeout = false
	elif fadein:
		if curr_frame < last_fadein_frame:
			curr_frame += 0.075
			modulate = Color(1,1,1, curr_frame)
		else:
			emit_signal("faded_in")
			curr_frame = 1
			fadein = false

func fade_out():
	modulate = Color(1,1,1,1)
	fadeout = true
	set_process(true)

func fade_in():
	modulate = Color(1,1,1,0)
	fadein = true
	set_process(true)

func _ready():
	randomize()
	Global.boss = self
	Global.boss_pos = position
	Global.boss_width = width
	Global.boss_height = height
	set_physics_process(false)
	$BatSprite.animation = "create"
	$BatSprite.playing = true
	yield($BatSprite,"animation_finished")
	$BatSprite.playing = false
	$BatSprite.animation = "default"
	is_hitable = true
	yield(get_tree().create_timer(0.83), "timeout")
	spawn_slaves()


func stop_and_hit():
	$Eyes.fade_in()
	$Eyes.visible = true

func spawn_slaves():
	$Shield.create()
	yield(get_tree().create_timer(0.05), "timeout")
	is_hitable = true
	yield(get_tree().create_timer(0.35), "timeout")
	var warning_created = false
	bat_slaves.clear()
	var pos = Global.Grid.find_nearest_cell(Global.silo.position)
	pos.y = Global.Grid.prev_cell_y(pos.y)
	for i in range(3):
		if not warning_created:
			var w = warning.instance()
			w.position.x = 0
			w.position.y = pos.y - 40
			get_parent().add_child(w)
			yield(w, "warned")
			warning_created = true
		var bs = bat_slave.instance()
		bs.position = pos
		bs.position.x = 440
		bs.rotation = Global.GridSpawner.monster_rotations[2]
		bs.velocity = Global.GridSpawner.monster_directions[2]
		bs.speed = rand_range(550, 650)
		get_parent().add_child(bs)
		bat_slaves.append(bs)
		pos.y += 80
	for bs in bat_slaves:
		bs.launch()
	$Shield.remove()
	is_hitable = true
	$t_fade_and_hit.start()

func _on_Eyes_faded():
	$Eyes.fast_fade = false
	modulate = Color(1,1,1, 1)
	yield(get_tree().create_timer(0.5), "timeout")
	var bh = blood_hit.instance()
	bh.position = player_position + Vector2(26.93, -21)
	get_parent().add_child(bh)
	if not hit3 or hit_times == 2:
		yield(get_tree().create_timer(0.6), "timeout")
		Global.background.fade_in()
		fade_in()
		$Eyes.visible = false
		$BatSprite.visible = true
		$health_sprite.visible = true
		yield(Global.background, "faded_in")
		$CollisionShape2D.disabled = false
		is_hitable = true
		yield(get_tree().create_timer(1), "timeout")
		spawn_slaves()
	else:
		yield(get_tree().create_timer(0.27), "timeout")
		$Eyes.fast_fade = true
		change_position_then_hit()
		hit_times += 1


func _on_t_fade_and_hit_timeout():
	Global.background.fade_out()
	fade_out()
	yield(Global.background, "faded_out")
	is_hitable = true
	$BatSprite.visible = false
	$health_sprite.visible = false
	$CollisionShape2D.disabled = true
	change_position_then_hit()

func change_position_then_hit():
	player_position = Global.silo.position
	if player_position.x <= 40:
		position = Vector2(120, player_position.y)
	elif  player_position.x >= 360:
		position = Vector2(280, player_position.y)
	else :
		position = Global.Grid.find_nearest_cell(player_position)
		var randpos = randi()%2
		if randpos:
			position.x = player_position.x - 80
		else:
			position.x = player_position.x + 80
	Global.boss_pos = position
	set_physics_process(true)
	stop_and_hit()


func _on_bat_boss_body_entered(body):
	body.hit()


func _on_bat_boss_mouse_entered():
	print("entered")


func _on_Shield_frame_changed():
	pass # Replace with function body.
