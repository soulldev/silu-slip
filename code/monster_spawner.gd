extends Node

var wave = 1
var passed_waves = 0
var chaos_counter = 1
var boss_counter = 1

func _ready():
	Global.Spawner = self
	yield(get_tree().create_timer(0.4), "timeout")
	Global.GameManager.next_stage()
	Global.GridSpawner.init_monsters()
	start_boss_wave()
	Global._set_input_method(false, true)

func start_grid_wave():
	Global.current_wave_type = "grid wave"
	Global.wave_text.play_wave_animation(wave)
	yield(Global.wave_text, "animation_played")
	$Grid_Spawner.start_wave()
	Global.GridSprite.create()

#func start_chaos_wave():
#	Global.GridSprite.remove()
#	yield(get_tree().create_timer(0.1), "timeout")
#	Global.big_wave_text.play_chaos()
#	Global._set_input_method(true, false)
#	Global.WaveText.play_chaos()
#	yield(Global.big_wave_text, "finished")
#	$Monsters_Spwner.start_chaos()

func start_boss_wave():
	Global.current_wave_type = "boss fight"
	$Boss_Spawner.spawn_boss()
	wave=1

func wave_end():
	passed_waves+=1
	Global.GameManager.update_counters()
#	if wave !=4:
#		start_grid_wave()
#		$Grid_Spawner/t_grid_passed_1sec.stop()
#	else:
	start_boss_wave()
