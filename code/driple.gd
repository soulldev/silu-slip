extends Area2D

var direction
var speed = 640

func _ready():
	set_physics_process(false)

func launch():
	set_physics_process(true)

func _physics_process(delta):
	position += direction.normalized() * speed * delta
	speed += 3

func _on_driple_area_entered(area):
	if area.name == "jelly_boss" or area.name == "bat_boss":
		area.health-=1
		if area.health==0:
			Global.boss_pos = Vector2(-1,-1)
			area.queue_free()
			Global.Spawner.wave_end()
			get_tree().call_group("points", "queue_free")
	else:
		speed = 0
		set_physics_process(false)
		yield(get_tree().create_timer(0.25), "timeout")
	queue_free()
