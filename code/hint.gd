extends AnimatedSprite

var side
var speed
var frames_count = 100

func _ready():
	set_physics_process(false)

func _on_Timer_timeout():
	queue_free()

func _physics_process(delta):
	if side=="UP":
		position.y += speed * delta
	elif side == "DOWN":
		position.y -= speed * delta
	elif side == "LEFT":
		position.x += speed * delta
	elif side == "RIGHT":
		position.x -= speed * delta
	frames_count-=1
	modulate = Color(1,1,1,frames_count*0.01)

func color(spd):
	if spd > 315:
		modulate = Color(1, 0, 0, 1)
		
func launch():
	set_physics_process(true)
	frame=1
	playing=false


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
