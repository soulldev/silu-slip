extends CanvasLayer

export (PackedScene) var anm_coin
func _ready():
	for i in range(10):
		Global.scr_panel = self
		$Control/scr_p.self_modulate = Color(1,1,1,i*0.1)
		$Control/coins.self_modulate = Color(1,1,1,i*0.1)
		yield(get_tree().create_timer(0.0025), "timeout")
	$Control/scr.text = str(0)
	$Control/scr_coins.text = str(Global.coins)


func show_score(score):
	yield(get_tree().create_timer(0.05), "timeout")
	for i in range(score):
		$Control/scr.text = str(i+1)
		$Control/coin.play()
		if score < 10:
			yield(get_tree().create_timer(0.088), "timeout")
		elif score >= 10 and score < 24:
			yield(get_tree().create_timer(0.074), "timeout")
		elif score >= 24 and score < 32:
			yield(get_tree().create_timer(0.064), "timeout")
		elif score >=32 and score < 47:
			yield(get_tree().create_timer(0.056), "timeout")
		else:
			yield(get_tree().create_timer(0.022), "timeout")
	$Control/scr.hide()
	$Control/scr_gold.text = str(score)
	$Control/scr_gold.show()
	$Control/VB/msg.text = "you survived for " + str(score) + " seconds"
	$Control/VB/msg2.text = "+" + str(score) + " coins!"
	show_button()
	Global.coins += score
	if score > Global.high_score:
		Global.high_score = score
	var cn = anm_coin.instance()
	get_parent().add_child(cn)
	cn.position = $Control/pos_coin.position
	cn.get_node("Label").text = "+" + str(score)
	$Control/scr_coins.text = str(Global.coins)

func show_button():
	$Control/Button.show()

func _on_Button_pressed():
	get_parent().start()
	queue_free()
