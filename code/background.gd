extends Sprite

var fadeout = true
var fadein = false
var last_fadeout_frame=0
var last_fadein_frame=1
var curr_frame = 1
signal faded_out
signal faded_in

func _ready():
	Global.background = self
	set_process(false)

func _process(delta):
	if fadeout:
		if curr_frame > last_fadeout_frame:
			curr_frame -= 0.05
			modulate = Color(1,1,1, curr_frame)
		else:
			set_process(false)
			emit_signal("faded_out")
			curr_frame = 0
			fadeout = false
	elif fadein:
		if curr_frame < last_fadein_frame:
			curr_frame += 0.075
			modulate = Color(1,1,1, curr_frame)
		else:
			set_process(false)
			emit_signal("faded_in")
			curr_frame = 1
			fadein = false

func fade_out():
	modulate = Color(1,1,1,1)
	fadeout = true
	set_process(true)

func fade_in():
	modulate = Color(1,1,1,0)
	fadein = true
	set_process(true)
