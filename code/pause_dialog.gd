extends TextureRect

var pressed_settings = false

func show():
	visible = true
	$X.disabled = false
	$settings.disabled = false
	$continue.disabled = false

func hide():
	visible = false
	$X.disabled = true
	$settings.disabled = true
	$continue.disabled = true

func _on_X_button_down():
	texture = load("res://sprt/GUI/Dialog_pause_x.png")


func _on_X_button_up():
	texture = load("res://sprt/GUI/Dialog_pause.png")

func _on_X_pressed():
	get_tree().quit()

func _on_settings_button_up():
	if not pressed_settings:
		texture = load("res://sprt/GUI/Dialog_pause.png")


func _on_settings_button_down():
	texture = load("res://sprt/GUI/Dialog_pause_settings.png")


func _on_settings_pressed():
	if not pressed_settings:
		texture = load("res://sprt/GUI/Dialog_pause_settings_pressed.png")
	else:
		texture = load("res://sprt/GUI/Dialog_pause.png")
	pressed_settings = !pressed_settings


func _on_continue_button_down():
	texture = load("res://sprt/GUI/Dialog_pause_play.png")


func _on_continue_button_up():
	texture = load("res://sprt/GUI/Dialog_pause.png")


func _on_continue_pressed():
	visible = false




func _on_pause_button_pressed():
	show()
