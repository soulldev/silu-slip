extends Node

onready var jelly_pink = load("res://scns/jelly_pink.tscn")
onready var jelly_yellow = load("res://scns/jelly_yellow.tscn")
onready var jelly_black = load("res://scns/jelly_black.tscn")
onready var hint = load("res://scns/hint.tscn")
var monsters = []
var boss_slave

# All the locations that monsters spawn upon
var all_mon_spawn_locations2 = {"DOWN":[Vector2(40,760), Vector2(120,760), Vector2(200,760), Vector2(280,760), Vector2(360,760)],
								"LEFT":[Vector2(40,120), Vector2(40,200), Vector2(40,280), Vector2(40,360), Vector2(40,440), Vector2(40,520), Vector2(40,600), Vector2(40,680)],
								"RIGHT":[Vector2(360,120), Vector2(360,200), Vector2(360,280), Vector2(360,360), Vector2(360,440), Vector2(360,520), Vector2(360,600), Vector2(360,680)]}

# used to know which side the current spawn on, there is no UP because no monsters spawn from there
var side_names = ["DOWN", "LEFT", "RIGHT"]

# to insert location, speed for each monster before spawning
var saved_locations = {"DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
var saved_speeds = {"DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}

# all rows, columns (components of vector2) that monsters will spawn on.
# needed to guarantee that there's a monster spawning near you.
var saved_x_locations = []
var saved_y_locations = []

# just for location generator
var opposite_side = ["NONE", "NONE", "LEFT"] #opposite of each side to eliminate opposites
export var numbers = [2, 2, 3] #numbers of monsters spawn at each side
var limits = [5, 8, 8] # the limit of each side

# directions, monster_rotations, speeds of monsters. directions and rotations are for each in side_names.
var monster_directions = [Vector2(0,-1), Vector2(1,0), Vector2(-1,0)]
var monster_rotations = [-PI/2, 0, PI]
export var green_min_speed = 170
export var green_max_speed = 190
export var red_min_speed = 285
export var red_max_speed = 330

func _ready():
	Global.GridSpawner = self
	set_process(false)
	set_physics_process(false)
	randomize()

func init_monsters():
	if Global.GameManager.current_stage == "jellyfishes":
		monsters = [jelly_pink, jelly_yellow]
		boss_slave = jelly_black
	
	
func start_wave():	
	Global.Grid.clear_elimination_all() #clears the rows and columns that was eliminated by monsters
	
	# clears saved locations and speeds from last wave
	saved_locations = {"DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
	saved_speeds = {"DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
	saved_x_locations = []
	saved_y_locations = []

	# Each loop, this code generates all locations and speeds in one side
	var player_hit_positions = 0
	var index = 0
	for i in side_names:
		generate_locations(i, saved_locations[i], limits[index], numbers[index], saved_locations.get(opposite_side[index]))
		generate_speeds(saved_locations[i], i, saved_speeds[i])
		index += 1
	
	# Sometimes the player remains in his position and pass the wave without effort.
	# to prevent this, the row where the player in must be eliminated
	if not saved_y_locations.has(Global.silo.position.y) and not saved_x_locations.has(Global.silo.position.x):
		spawn_towards_player(side_names[randi()%3])

	# spawn the monsters and the hints for each side
	# After this code ends, both spawned monsters and spawned hints are exist, but they both won't be shown.
	# the hints animation are on the first empty frame, the monsters are outside the viewport without moving.
	for i in range(3):
		var side = side_names[i]
		var rot = monster_rotations[i]
		var velocity = monster_directions[i]
		var spawn = spawn_monsters(monsters[randi()%3], saved_locations[side],saved_speeds[side], side, rot, velocity)
		if spawn is GDScriptFunctionState:
			var state = spawn
			state = yield(state, "completed")
		Global.mon_collidable=true
		yield(get_tree().create_timer(0.01), "timeout")
		#Spawn hints: I use many yields for better performance, so thier is no sudden fps drop
		var hints = spawn_hints(side, saved_locations[side], saved_speeds[side], side)
		if hints is GDScriptFunctionState:
			var state = hints
			state = yield(state, "completed")
		yield(get_tree().create_timer(0.01), "timeout")

	#Animate All hints one at a time, wait for a delay (so the player will figure out what he needs to do) then launch all enemies with thier speeds
	animate_hints()
	yield(get_tree().create_timer(1.15), "timeout")
	launch_hints()
	yield(get_tree().create_timer(0.253), "timeout")
	get_tree().call_group("enemies", "eliminate")
	yield(get_tree().create_timer(0.653), "timeout")
	launch_monsters()
	Global.can_end_wave=true
	Global.mon_collidable=false
	increase_difficulty()
	get_parent().wave+=1

func start_wave_for_boss():	
	Global.Grid.clear_elimination_all() #clears the rows and columns that was eliminated by monsters
	# clears saved locations and speeds from last wave
	saved_locations = {"DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
	saved_speeds = {"DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
	saved_x_locations = []
	saved_y_locations = []

	# Each loop, this code generates all locations and speeds in one side
	var player_hit_positions = 0
	var index = 0
	
	spawn_towards_player("DOWN", true)
	spawn_towards_player("LEFT", true)
	spawn_towards_player("RIGHT", true)

	for i in range(3):
		var side = side_names[i]
		var rot = monster_rotations[i]
		var velocity = monster_directions[i]
		var spawn = spawn_monsters(boss_slave, saved_locations[side],saved_speeds[side], side, rot, velocity)
		if spawn is GDScriptFunctionState:
			var state = spawn
			state = yield(state, "completed")
		Global.mon_collidable=true

	get_tree().call_group("enemies", "eliminate")
	launch_monsters()

func generate_locations(side , list , limit, number, opposite):
	for i in range(number):
		var loc = randi()%limit
		var location = all_mon_spawn_locations2[side][loc]
		var guarantee = 0
		var identical_opposite = null
		if side=="RIGHT":
			for el in opposite:
				if el.y==location.y:
					identical_opposite=el
		while (list.has(location) or location==identical_opposite) and guarantee < pow(limit, 4):
			loc = randi()%limit
			location = all_mon_spawn_locations2[side][loc]
			guarantee += 1
		if not list.has(location) and not location==identical_opposite:
			list.insert(list.size(), location)
		saved_y_locations.insert(0, all_mon_spawn_locations2[side][loc].y)
		saved_x_locations.insert(0, all_mon_spawn_locations2[side][loc].x)

func generate_speeds(side, list, saved_spd):
	var index = 0
	for i in list:
		var randSpeedGenre = randi()%4
		if randSpeedGenre==0 or randSpeedGenre==1:
			saved_spd.insert(saved_spd.size(), rand_range(green_min_speed, green_max_speed))
		else:
			saved_spd.insert(saved_spd.size(), rand_range(red_min_speed, red_max_speed))

func spawn_monsters(type, list,spds, side_name, rot, velocity):
	var index = 0
	for i in list:
		yield(get_tree().create_timer(0.02), "timeout")
		var mon = type.instance()
		add_child(mon)
		mon.position = i
		if side_name=="LEFT":
			mon.position.x-=80
		elif side_name=="RIGHT":
			mon.position.x+=80
		elif side_name=="DOWN":
			mon.position.y+=80
		mon.rotation = rot
		mon.velocity = velocity
		if mon.velocity.x!=0:
			Global.Grid.eliminate_row(mon.position.y)
		elif mon.velocity.y!=0:
			Global.Grid.eliminate_column(mon.position.x)
		mon.speed = spds[index]
		index += 1
		yield(get_tree().create_timer(0.02), "timeout")

#can make hints colored regarding of the speed
func spawn_hints(side_name, list, spds, side_name):
	var index = 0
	for i in list:
		yield(get_tree().create_timer(0.02), "timeout")
		var hnt = hint.instance()
		add_child(hnt)
		hnt.position = i
		if side_name=="DOWN":
			hnt.position.y += 15
		if (spds[index]>=green_min_speed&&spds[index]<green_max_speed):
			hnt.animation="green"
		else:
			hnt.animation="red"
		hnt.side = side_name
		hnt.speed = spds[index] * 2.94
		index += 1
		yield(get_tree().create_timer(0.02), "timeout")

func launch_monsters():
	get_tree().call_group("enemies", "call_deferred", "launch")

func launch_hints():
	get_tree().call_group("hint", "call_deferred", "launch")

func animate_hints():
	get_tree().set_group("hint", "playing", true)

func increase_difficulty():
	var DownOrNot = randi()%2
	if DownOrNot:
		if numbers[0] < 3:
				numbers[0] += 1
	else:
		var i = randi()%2 + 1
		if numbers[i] < 4 :
			numbers[i] += 1
	if get_parent().passed_waves<5 and !numbers.has(0):
		var randz = randi()%3
		numbers[randz]=0

func _on_t_speedup_timeout():
	set_process(true)

func spawn_towards_player(side, custom_speed=false):
	for pos in all_mon_spawn_locations2[side]:
		var player_component
		var match_component
		
		if side=="DOWN":
			player_component = Global.silo.position.x
			match_component = pos.x
		else:
			player_component = Global.silo.position.y
			match_component = pos.y
		
		if match_component == player_component:
			saved_locations[side].insert(saved_locations[side].size(), pos)
		add_random_speed(side, custom_speed)

func add_random_speed(side, custom_speed):
		var randSpeedGenre = randi()%2
		if randSpeedGenre==0 or custom_speed:
			var green_speed = rand_range(green_min_speed, green_max_speed)
			if custom_speed:
				green_speed -= 30
			saved_speeds[side].insert(saved_speeds[side].size(), green_speed)
		elif randSpeedGenre==1:
			saved_speeds[side].insert(saved_speeds[side].size(), rand_range(red_min_speed, red_max_speed))
