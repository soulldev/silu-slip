extends Node

var start_location
var start_location2
var last_location
var last_direction = Vector2(5,5)
var released
var slip_started
var notPressed = true
var vec

signal slip

func _ready():
	connect("slip",Global.silo,"_on_SlipDetector_slip")

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and Global.boss_pos != Vector2(-1,-1) and Global.boss.is_hitable and event.position.x >= Global.boss_pos.x - Global.boss_width/2 and event.position.x <= Global.boss_pos.x + Global.boss_width/2 and event.position.y >= Global.boss_pos.y - Global.boss_height/2 and event.position.y <= Global.boss_pos.y + Global.boss_height/2:
			if not Global.silo.attacking:
				Global.silo.rotate_to_attack()
			return
		if event.pressed:
			released = false
			start_location = get_viewport().get_mouse_position()
		if not event.pressed:
			released = true
		if not released:
			_updatepos()


func _updatepos():
	while not released:
		yield(get_tree().create_timer(0.07),"timeout")
		last_location = get_viewport().get_mouse_position()
		vec = last_location - start_location
		if vec.length() > 15.3: #9.3 for normal mode
			if abs(vec.x) > abs(vec.y):
				_sendpos(Vector2(sign(vec.x), 0.0))
			else:
				_sendpos(Vector2(0.0, sign(vec.y)))
		if vec.length() < 34:
			start_location = last_location
	if released == true:
			_sendpos(Vector2())

func _sendpos(direction):
	if direction == Vector2() or Global.silo.last_direction != direction:
		emit_signal("slip", direction)
