extends AnimatedSprite

var numof_frames = 9
var first_frame = 0

func _ready():
	playing = true

func _on_bloodhit_claws_animation_finished():
	if position == Global.silo.position :
		print ("pos matches")
		Global.silo.hit()


func _on_bloodhit_claws_frame_changed():
	if position == Global.silo.position + Vector2(26.93, -21) :
		Global.silo.can_move=false
		yield(get_tree().create_timer(0.2), "timeout")
		Global.silo.can_move=true
		Global.silo.hit()
	if frame >= numof_frames/2:
		$Area2D/CollisionShape2D.disabled = false
	if frame == numof_frames:
		yield(get_tree().create_timer(0.2), "timeout")
		play("blood", true)
	elif frame == first_frame:
		queue_free()


func _on_Area2D_body_entered(body):
	body.hit()
