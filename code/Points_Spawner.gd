extends Node

onready var point = load("res://scns/point_waterdriple.tscn")


func _ready():
	randomize()
	Global.PointsSpawner = self


func spawn():
	if get_tree().get_nodes_in_group("points").size() < 2:
		var p = point.instance()
		while true :
			p.global_position.x = rand_range(20,380)
			p.global_position.y = rand_range(30,770)
			if ( Global.boss.global_position - p.global_position ).length() > 180 :
				break;
		Global.main.add_child(p)
