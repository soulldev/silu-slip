extends Node

var start_location
var last_location
var last_direction = Vector2(5,5)
var released
var x = true
var vec
signal swipe

func _ready():
	connect("swipe",Global.silo,"_on_SwipeDetector_swipe")

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			released = false
			start_location = get_viewport().get_mouse_position()
		if not event.pressed:
			x = true
			released = true
	if event is InputEventMouseButton and x:
		if not released:
			x = false
			_updatepos()

func _updatepos():
	while not released:
		yield(get_tree().create_timer(0.07),"timeout")
		last_location = get_viewport().get_mouse_position()
		vec = last_location - start_location
		if vec.length() > 9.3:
			if abs(vec.x) > abs(vec.y):
				_sendpos(Vector2(-sign(vec.x), 0.0))
			else:
				_sendpos(Vector2(0.0, -sign(vec.y)))
		start_location = last_location
	if released == true:
		_sendpos(Vector2())

func _sendpos(direction):
	if last_direction == Vector2(5,5):
		emit_signal("swipe", direction)
		$Timer.start()
	elif direction == Vector2():
		emit_signal("swipe", direction)
	last_direction = direction

func _on_Timer_timeout():
	last_direction = Vector2(5,5)
