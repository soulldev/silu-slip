extends Node

export (PackedScene) var jelly_blue
export (PackedScene) var jelly_pink
export (PackedScene) var jelly_yellow
export (PackedScene) var bat
onready var hint = load("res://scns/hint.tscn")
var monsters = []
var toward_player_side
var spawned_toward_player = false
var numof_mon_types

# All the positions that monsters spawn upon, this wont change at all
const all_possible_spawn_positions = {"DOWN":[Vector2(40,760), Vector2(120,760), Vector2(200,760), Vector2(280,760), Vector2(360,760)],
								"LEFT":[Vector2(40,120), Vector2(40,200), Vector2(40,280), Vector2(40,360), Vector2(40,440), Vector2(40,520), Vector2(40,600), Vector2(40,680)],
								"RIGHT":[Vector2(360,120), Vector2(360,200), Vector2(360,280), Vector2(360,360), Vector2(360,440), Vector2(360,520), Vector2(360,600), Vector2(360,680)]}

var current_possible_spawn_positions = {}
var numberof_spawn_positions 
var spawn_positions = []
var spawn_y_positions = []
var spawn_x_positions = []

# used to know which side the current spawn on, there is no UP because no monsters spawn from there
var side_names = ["DOWN", "LEFT", "RIGHT"]

# directions, monster_rotations, speeds of monsters. directions and rotations are for each in side_names.
var monster_directions = [Vector2(0,-1), Vector2(1,0), Vector2(-1,0)]
var monster_rotations = [-PI/2, 0, PI]
export var green_min_speed = 170
export var green_max_speed = 190
export var red_min_speed = 285
export var red_max_speed = 330

func _ready():
	Global.GridSpawner = self
	set_process(false)
	set_physics_process(false)
	randomize()

func init_monsters():
	if Global.GameManager.current_stage == "jellyfishes":
		monsters = [jelly_blue, jelly_pink, jelly_yellow]
		numof_mon_types = 3
	elif Global.GameManager.current_stage == "dark bats":
		monsters = [bat]
		monster_rotations = [0, 0, PI]
		numof_mon_types = 1

func generate_spawn_position(side):
	var spawn_pos
	var index
	if not spawned_toward_player and toward_player_side == side:
		var i = 0
		for position in current_possible_spawn_positions[side]:
			if position.x == Global.silo.position.x or position.y == Global.silo.position.y:
				spawn_pos = position
				spawned_toward_player = true
				index = i
				break
			i += 1
	else:
		var number_of_possible_positions = current_possible_spawn_positions[side].size()
		index = randi()%number_of_possible_positions
		spawn_pos = current_possible_spawn_positions[side][index]
	current_possible_spawn_positions[side].remove(index)
	if side=="LEFT":
		current_possible_spawn_positions["RIGHT"].remove(index)
	return spawn_pos

func start_wave():
	spawn_positions.clear()
	spawn_x_positions.clear()
	spawn_y_positions.clear()
	current_possible_spawn_positions = {"DOWN":[Vector2(40,760), Vector2(120,760), Vector2(200,760), Vector2(280,760), Vector2(360,760)],
								"LEFT":[Vector2(40,120), Vector2(40,200), Vector2(40,280), Vector2(40,360), Vector2(40,440), Vector2(40,520), Vector2(40,600), Vector2(40,680)],
								"RIGHT":[Vector2(360,120), Vector2(360,200), Vector2(360,280), Vector2(360,360), Vector2(360,440), Vector2(360,520), Vector2(360,600), Vector2(360,680)]}
	numberof_spawn_positions = get_parent().wave + 3
	spawned_toward_player = false
	toward_player_side = side_names[randi()%2]
	if Global.silo.position.y == 40:
		toward_player_side = "DOWN"
	var index = 0
	for side in side_names:
		var numof_spawn_positions_in_side
		if side=="DOWN":
			numof_spawn_positions_in_side = randi()%3 + 1
			numberof_spawn_positions -= numof_spawn_positions_in_side
		elif side=="LEFT":
			numof_spawn_positions_in_side = randi()%3 + 2
			numberof_spawn_positions -= numof_spawn_positions_in_side
		else:
			numof_spawn_positions_in_side = numberof_spawn_positions
		for i in range(numof_spawn_positions_in_side):
			var rotation = monster_rotations[index]
			var direction = monster_directions[index]
			var type = randi()%numof_mon_types
			var speed = randi()%2
			if speed == 0:
				speed = rand_range(green_min_speed, green_max_speed)
			else:
				speed = rand_range(red_min_speed, red_max_speed)
			var mon = monsters[type].instance()
			add_child(mon)
			var pos = generate_spawn_position(side)
			mon.position = pos
			if side=="LEFT":
				mon.position.x-=80
			elif side=="RIGHT":
				mon.position.x+=80
			elif side=="DOWN":
				mon.position.y+=80
			mon.rotation = rotation
			mon.velocity = direction
			if mon.velocity.x!=0:
				Global.Grid.eliminate_row(mon.position.y)
			elif mon.velocity.y!=0:
				Global.Grid.eliminate_column(mon.position.x)
			mon.speed = speed
			var hnt = hint.instance()
			add_child(hnt)
			hnt.position = pos
			if side=="DOWN":
				hnt.position.y += 15
			if (speed>=green_min_speed&&speed<green_max_speed):
				hnt.animation="green"
			else:
				hnt.animation="red"
			hnt.side = side
			hnt.speed = speed * 2.94
		index += 1
	Global.mon_collidable=true
	animate_hints()
	yield(get_tree().create_timer(1.15), "timeout")
	launch_hints()
	yield(get_tree().create_timer(1.15), "timeout")
	launch_monsters()
	Global.can_end_wave=true
	Global.mon_collidable=false
	get_parent().wave += 1

func launch_monsters():
	get_tree().call_group("enemies", "call_deferred", "launch")

func animate_hints():
	get_tree().set_group("hint", "playing", true)

func launch_hints():
	get_tree().call_group("hint", "call_deferred", "launch")
