extends Node2D

signal warned

func _ready():
	visible = true
	yield(get_tree().create_timer(0.25), "timeout")
	visible = false
	yield(get_tree().create_timer(0.25), "timeout")
	visible = true
	yield(get_tree().create_timer(0.25), "timeout")
	visible = false
	yield(get_tree().create_timer(0.25), "timeout")
	emit_signal("warned")
	queue_free()
