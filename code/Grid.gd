extends Node

var grid
var grids_status = {Vector2(40,40):false, Vector2(120, 40):false, Vector2(200, 40):false, Vector2(280, 40):false, Vector2(360, 40):false, Vector2(40,120):false, Vector2(120, 120):false, Vector2(200, 120):false, Vector2(280, 120):false, Vector2(360, 120):false, Vector2(40,200):false, Vector2(120, 200):false, Vector2(200, 200):false, Vector2(280, 200):false, Vector2(360, 200):false, Vector2(40,280):false, Vector2(120, 280):false, Vector2(200, 280):false, Vector2(280, 280):false, Vector2(360, 280):false, Vector2(40,360):false, Vector2(120, 360):false, Vector2(200, 360):false, Vector2(280, 360):false, Vector2(360, 360):false, Vector2(40,440):false, Vector2(120, 440):false, Vector2(200, 440):false, Vector2(280, 440):false, Vector2(360, 440):false, Vector2(40,520):false, Vector2(120, 520):false, Vector2(200, 520):false, Vector2(280, 520):false, Vector2(360, 520):false, Vector2(40,600):false, Vector2(120, 600):false, Vector2(200, 600):false, Vector2(280, 600):false, Vector2(360, 600):false, Vector2(40,680):false, Vector2(120, 680):false, Vector2(200, 680):false, Vector2(280, 680):false, Vector2(360, 680):false, Vector2(40,760):false, Vector2(120, 760):false, Vector2(200, 760):false, Vector2(280, 760):false, Vector2(360, 760):false}
var uneliminated_rows

func _ready():	
	Global.Grid = self
	grid = create_2d_array(10, 5)

func has_cell(pos):
	return grids_status.has(pos)

func find_nearest_cell(pos):
	var nearest_x=40
	var nearest_y=40
	var found = null
	var distance = 41
	while found==null:
		for i in range(10):
			for i in range(5):
				if ((pos-Vector2(nearest_x, nearest_y)).length() <= distance):
					found = Vector2(nearest_x, nearest_y)
					break
				nearest_x+=80
			nearest_y+=80
			nearest_x=40
		distance+=10
		nearest_x=40
		nearest_y=40
	return found

func grids_between(pos1, pos2):
	pos1-=40
	pos2-=40
	return (abs(pos2-pos1))/80

func eliminate_row(y):
	var start_pos = Vector2(40,y)
	for i in range(5):
		grids_status[start_pos+Vector2(i*80,0)] = true

func eliminate_column(x):
	var start_pos = Vector2(x,40)
	for i in range(10):
		grids_status[start_pos+Vector2(0,i*80)] = true

func clear_elimination_all():
	var start_pos=Vector2(40,40)
	for i in range(10):
		for i in range(5):
			grids_status[start_pos] = false
			start_pos.x+=80
		start_pos.y+=80
		start_pos.x=40

func create_2d_array(rows, columns):
	var a = []
	var X=40
	var Y=40
	for row in range(rows):
		X=40
		a.append([])
		a[row].resize(columns)
		for column in range(columns):
			a[row][column] = Vector2(X,Y)
			X+=80
		Y+=80
	return a

func middle_position():
	return grid[4][2] 

func next_x(current_x):
	return current_x+80

func next_cell_x(current_x):
	var next_cell = current_x+80
	if exceeded_x_right(next_cell):
		next_cell = 40
	return next_cell

func prev_cell_x(current_x):
	var prev_cell = current_x-80
	if exceeded_x_left(prev_cell):
		prev_cell = 360
	return prev_cell

func prev_cell_y(current_y):
	var prev_cell = current_y-80
	if exceeded_y_up(prev_cell):
		prev_cell = 760
	return prev_cell

func next_cell_y(current_y):
	var next_cell = current_y+80
	if exceeded_y_down(next_cell):
		next_cell = 40
	return next_cell

func prev_x(current_x):
	return current_x-80

func next_y(current_y):
	return current_y+80

func prev_y(current_y):
	return current_y-80

func exceeded_x_right(current_x):
	return current_x>grid[0][4].x

func exceeded_x_left(current_x):
	return current_x<grid[0][0].x

func exceeded_y_up(current_y):
	return current_y<grid[0][0].y

func exceeded_y_down(current_y):
	return current_y>grid[9][0].y

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
