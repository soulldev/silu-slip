extends AnimatedSprite

var last_frame

func _ready():
	last_frame=frames.get_frame_count("1_fade")-1
	playing = true

func _on_boss_summon_animation_frame_changed():
	if animation=="1_fade" and frame==last_frame:
		queue_free()


func _on_boss_summon_animation_animation_finished():
	play("1_fade")
