extends Area2D

var velocity
var speed

func _ready():
	randomize()
	set_process_unhandled_input(false)
	set_process_input(false)
	set_process_priority(false)
	set_process_unhandled_key_input(false)
	add_to_group("enemies")
	set_physics_process(false)

var final_speedup
var speedup = false

func _physics_process(delta):
	position += velocity * speed * delta
	if speedup and speed<final_speedup:
		speed += 10

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func die():
	for i in range(5):
		$Animation.self_modulate = Color(1, 1, 1, 1-(i*0.2))
		yield(get_tree().create_timer(0.03), "timeout")
	queue_free()

func cont(s):
	speed = s

func speed_up():
	final_speedup=speed*3
	speedup=true

func eliminate():
	if velocity.y != 0:
		Global.Grid.eliminate_column(position.x)
	elif velocity.x != 0:
		Global.Grid.eliminate_row(position.y)

func stop():
	speed=0

func _on_jelly_body_entered(body):
	if not (Global.Grid.exceeded_x_right(Global.silo.position.x) or Global.Grid.exceeded_x_left(Global.silo.position.x) or Global.Grid.exceeded_y_down(Global.silo.position.y) or Global.Grid.exceeded_y_up(Global.silo.position.y)):
			var s = speed
			get_tree().call_group("enemies", "set_physics_process", false)
			body.hit()

func launch():
	set_physics_process(true)

func _on_Octopus_tree_entered():
	Global.mon_num+=1

func _on_Octopus_tree_exited():
	Global.mon_num-=1

func _on_t_grid_1sec_passed_timeout():
	Global.grid_wave_passed_1_sec=true

func _on_wave_end_timer_timeout():
	Global.can_end_wave=true
	Global.emit_signal("can_end_wave")
