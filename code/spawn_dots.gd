extends AnimatedSprite

var numof_create_frames = 41
var first_frame = 0

func _ready():
	connect("animation_finished", self, "create_animation_finished")

func remove():
	play("remove")
	connect("animation_finished", self, "remove_animation_finished")

func default():
	disconnect("animation_finished", self, "create_animation_finished")
	play("default")

func remove_animation_finished():
	queue_free()

func create_animation_finished():
	default()
