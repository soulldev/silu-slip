extends Control

signal start

func _ready():
	Global.ghud = self

func add_score(score):
	$score.text = str(score)

func show_message(txt):
	$msg.text = str(txt)

func show_button():
	$Button.show()
	$msg.show()

func _on_Button_pressed():
	$Button.hide()
	$msg.hide()
	emit_signal("start")
