extends AnimatedSprite

var backwards = false

func _ready():
	yield(get_tree().create_timer(0.1),"timeout")
	Global.GridSprite=self

func create():
	if frame==0:
		backwards=false
		speed_scale = 1
		play("default", backwards)

func remove():
	if frame==10:
		backwards=true
		speed_scale = 0.8
		play("default", backwards)

func _on_scene_grid_animation_finished():
	playing=false
	if backwards:
		frame=0
	else:
		frame=10
