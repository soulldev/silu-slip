extends Node2D

export (PackedScene) var ook
export (PackedScene) var monster
export (PackedScene) var hint

var mon_num = {"number_left" : 1, "number_right":1, "number_up":1, "number_down":1}
var saved_locations = { "LEFT": [],"RIGHT":[], "UP":[],"DOWN":[], "NONE":[] }
var node_name = ["LEFT", "RIGHT", "UP", "DOWN"]
var rev_node_name = ["NONE", "LEFT", "NONE", "UP"]
var node_size = [10, 10, 5, 5]
var wave = 1
var cwave = 1
var min_monnumber = 1
var max_monnumber = 1
var min_speed = 240
var max_speed = 260
var ok_loc
var delay = 0.8
func _ready():
	randomize()

func _on_Timer_timeout():
	get_tree().call_group("hints", "queue_free")
	$Label.text = str(wave)
	if not cwave == 22:	
		if delay > 0.25:
			delay-=0.05
		saved_locations["llocation_left"] = []
		saved_locations["llocation_up"] = []
		saved_locations["llocation_down"] = []
		saved_locations["llocation_right"] = []
		
#		var randnode = randi()%4 + 1
#		if randnode ==1:
#			ok_loc = ["UP", "LEFT"]
#			if mon_num["number_up"] == 1 and mon_num["number_left"]==1:
#				mon_num["number_up"] += 1
#				mon_num["number_left"] += 2
#		elif randnode ==2:
#			ok_loc = ["DOWN", "RIGHT"]
#			if mon_num["number_down"] == 1 and mon_num["number_right"]==1:
#				mon_num["number_up"] += 1
#				mon_num["number_right"] += 2
#		elif randnode ==3:
#			ok_loc = ["DOWN", "LEFT"]
#			if mon_num["number_down"] == 1 and mon_num["number_left"]==1:
#				mon_num["number_down"] += 1
#				mon_num["number_left"] += 2
#		else:
#			ok_loc = ["UP", "RIGHT"]
#			if mon_num["number_up"] == 1 and mon_num["number_right"]==1:
#				mon_num["number_up"] += 1
#				mon_num["number_right"] += 2

		for i in range(node_name.size()):
			gen_locations(saved_locations[node_name[i]], node_size[i], node_name[i], rev_node_name[i])
			print(str(node_name[i]) + ":")
			for i in range(2):
				var x = node_name[i]
				print(saved_locations.get(x)[0])
#		# difficulty choice 1
#		yield (get_tree().create_timer(delay), "timeout")
#		lunch_monsters()
#		#$t_difficulty.start()
#		$t_wave.start()
	else:
		
		get_node("Monsters_Spwner").get_node("t_monspawn").start()
		yield (get_tree().create_timer(10), "timeout")
		get_node("Monsters_Spwner").get_node("t_monspawn").stop()
		var difficulty_choice = randi()%2
		if difficulty_choice:
			if $Monsters_Spwner.mon_minspeed < 246:
				$Monsters_Spwner.mon_minspeed+=4
			if $Monsters_Spwner.mon_maxspeed < 316:
				$Monsters_Spwner.mon_maxspeed+=8
		else:
			if $Monsters_Spwner.get_node("t_monspawn").wait_time > 0.28:
				$Monsters_Spwner.get_node("t_monspawn").wait_time -= 0.04
	wave += 1
	cwave %= 4
	cwave += 1

func  gen_locations(llocation, l_number, node, opposite_node):
	var location = randi()%l_number + 1
	var guarantee = 1
	while llocation.find(location) != -1 and guarantee < (l_number*l_number):
			location = randi()%l_number + 1
			guarantee += 1
	if guarantee < (l_number*l_number):
		llocation.insert(llocation.size(), location)


func spawn_in(location, vel, node, rotation, pos_okx, pos_oky):
	var mon_num = rand_range(min_monnumber, max_monnumber+1)
	for i in range (mon_num):
		var mon = monster.instance()
		add_child(mon)
		mon.position = get_node(node).get_node(str(location)).position
		if i != 0:
			mon.position.x += pos_okx
			mon.position.y += pos_oky
		mon.rotation = rotation
		mon.velocity = vel
		mon.speed = rand_range(min_speed, max_speed)
		yield (get_tree().create_timer(0.01), "timeout")
		#if node == ok_loc[0] or node == ok_loc[1]:
		#	var o_killer = ook.instance()
		#	add_child(o_killer)
		#	o_killer.position = get_node("ok_"+node).get_node(str(location)).position
		#	o_killer.vel = mon.velocity
	#yield (get_tree().create_timer(0.01), "timeout")
	#var hnt = hint.instance()
	#add_child(hnt)
	#hnt.position = get_node("ok_"+node).get_node(str(location)).position
	#hnt.velocity = vel

func lunch_monsters():
	get_tree().call_group("hints", "queue_free")
	var allmon = get_tree().get_nodes_in_group("enemys")
	for i in range(allmon.size()):
		allmon[i].set_physics_process(true)
		yield (get_tree().create_timer(0.005), "timeout")
	

#func _on_t_difficulty_timeout():
#	var difficulty_choice = randi()%14 + 1
#	if difficulty_choice < 5 or difficulty_choice > 10:
#		var x = randi()%4+1
#		var modchoice = randi()%3 +1
#		if x==1:
#			var smodchoice = randi()%5+1
#			if smodchoice !=4:
#				number_down %= 3
#			else:
#				number_down %= 4
#			number_down += 1
#		elif x==2:
#			if modchoice ==1:
#				number_left %= 4
#			else:
#				number_left %= 5
#			number_left += 1
#		elif x==3:
#			var smodchoice = randi()%5+1
#			if smodchoice !=4:
#				number_up %= 3
#			else:
#				number_up %= 4
#			number_up += 1
#		elif x==4:
#			if modchoice ==1:
#				number_right %= 4
#			elif modchoice ==2:
#				number_right %= 5
#			elif modchoice == 3:
#				number_right %= 3
#			number_right += 1
#	elif difficulty_choice == 5:
#		max_monnumber %= 2
#		max_monnumber += 1
#	else:
#		if difficulty_choice == 6:
#			if min_speed < 255:
#				min_speed += 6
#			if max_speed < 323:
#				max_speed += 8
#		elif difficulty_choice ==7:
#			if min_speed < 255:
#				min_speed += 9
#			if max_speed < 320:
#				max_speed += 11
#		elif difficulty_choice ==8:
#			if min_speed < 250:
#				max_speed += 16
#		elif difficulty_choice ==9:
#			if min_speed < 250:
#				min_speed += 16
#		else:
#			if min_speed < 260:
#				min_speed += 8
#			if max_speed < 320:
#				max_speed += 10
#	$t_wave.start()
