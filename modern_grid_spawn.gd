extends Node

export (PackedScene) var monster
export (PackedScene) var hint

var all_mon_spawn_locations2 = {"UP":[Vector2(40,40), Vector2(120,40), Vector2(200,40), Vector2(280,40), Vector2(360,40)],
							"DOWN":[Vector2(40,760), Vector2(120,760), Vector2(200,760), Vector2(280,760), Vector2(360,760)],
							"LEFT":[Vector2(40,120), Vector2(40,200), Vector2(40,280), Vector2(40,360), Vector2(40,440), Vector2(40,520), Vector2(40,600), Vector2(40,680)],
							"RIGHT":[Vector2(360,120), Vector2(360,200), Vector2(360,280), Vector2(360,360), Vector2(360,440), Vector2(360,520), Vector2(360,600), Vector2(360,680)]}

var saved_rows = []
var saved_columns = []
var saved_locations = {"UP" : [], "DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
var saved_speeds = {"UP" : [], "DOWN" : [], "LEFT" : [], "RIGHT" : [], "NONE" : []}
var node_names = ["UP", "DOWN", "LEFT", "RIGHT"]
#just for location generator
var opposite_node = ["NONE", "UP", "NONE", "LEFT"]
var numbers = [0, 2, 2, 1]
var limits = [5, 5, 8, 8]
#for monsters
var velocities = [Vector2(0,1), Vector2(0,-1), Vector2(1,0), Vector2(-1,0)]
var rotations = [PI/2, -PI/2, 0, PI]
export var green_min_speed = 170
export var green_max_speed = 190
export var red_min_speed = 285
export var red_max_speed = 330
export var speed_increase = 15
export var initial_v = 3
export var initial_h = 4


var last_player_pos

func _ready():
	set_physics_process(false)
	randomize()

func start_wave():	
	print("wave started")
	Global.Grid.clear_elimination_all()
	
	saved_rows = []
	saved_columns = []
#Each loop, this code generates all locations in one node, it will also generate speed for each location
	var player_hit_positions = 0
	var index = 0 #used as index in arrays(not dictionaries) like numbers, opposite_node
	for i in node_names:
		if get_parent().wave != 1:
			saved_locations[i].clear() #deletes all spawn locations in each wave
			saved_speeds[i].clear()
		generate_locations(i, saved_locations[i], limits[index], numbers[index], saved_locations.get(opposite_node[index]))
		generate_speeds(saved_locations[i], saved_speeds[i])
		index += 1
	var pos = Global.silo.position
	while Global.Grid.grids_status.has(pos) and Global.Grid.grids_status[pos]==false:
		index = randi()%4
		var i = node_names[index]
		saved_locations[i].clear() #deletes all spawn locations in each wave
		saved_speeds[i].clear()
		generate_locations(i, saved_locations[i], limits[index], numbers[index], saved_locations.get(opposite_node[index]))
		generate_speeds(saved_locations[i], saved_speeds[i])
	#initial difficulty: one of up or down number are 3, one of left or right are 4
	var initial = randi()%2
	numbers[initial] = initial_v
	initial = randi()%2 +2
	numbers[initial] = initial_h

#Each loop, this code spawn monsters in all locations in a node, this code will also spawn hints
#After this code ends, both spawned monsters and spawned hints are exist, but they both won't be shown, the hints are empty frame, the monsters are stopped outside the viewport
	for i in range(4):
		var node = node_names[i]
		var rot = rotations[i]
		var velocity = velocities[i]
		var spawn = spawn_monsters(saved_locations[node],saved_speeds[node], node, rot, velocity)
#		# normal yield with a complete signal didn't work, maybe their is a bug..i got this way of doing it from the godot QA
		if spawn is GDScriptFunctionState:
			var state = spawn
			state = yield(state, "completed")
		yield(get_tree().create_timer(0.01), "timeout")
		#Spawn hints: I use many yields for better performance, so thier is no sudden fps drop, maybe c++ will help increasing the performance
		var hints = spawn_hints(saved_locations[node], saved_speeds[node], node)
		if hints is GDScriptFunctionState:
			var state = hints
			state = yield(state, "completed")
		yield(get_tree().create_timer(0.01), "timeout")

	#Animate All hints one at a time, wait for a delay (so the player will figure out what he needs to do) then launch all enemies with thier speeds
	animate_hints()
	yield(get_tree().create_timer(1.3), "timeout")
	launch_hints()
	yield(get_tree().create_timer(0.253), "timeout")
	get_tree().call_group("enemies", "eliminate")
	yield(get_tree().create_timer(0.653), "timeout")
	last_player_pos=Global.silo.position
	launch_monsters()
	increase_difficulty()
	get_parent().wave+=1

func _process(delta):
	set_process(false)
	var pos = Global.silo.position
	if Global.Grid.grids_status.has(pos) and Global.Grid.grids_status[pos]==false:
		get_tree().call_group("enemies", "speed_up")

#opposed is the **list** of the parallel node, if list is up, opposite will be down
func generate_locations(node , list , limit, number, opposite):
	for i in range(number):
		var loc = randi()%limit
		var guarantee = 0
		while list.has(loc) and guarantee < limit*limit:
			loc = randi()%limit
			guarantee += 1
		if guarantee < limit*limit - 1:
			list.insert(list.size(), loc)
		if opposite.has(loc):
			print("opposite found")
			var delete_choice = randi()%2
			if not delete_choice:
				list.erase(loc)
			else:
				opposite.erase(loc)
		if list.size()>2 and (node=="UP" or node=="DOWN"):
			list.pop_back()
		if list.size()>4 and (node=="LEFT" or node=="RIGHT"):
			list.pop_back()
		if node=="UP" or node=="DOWN":
			Global.Grid.eliminate_row(all_mon_spawn_locations2[node][loc].y)
		elif node=="LEFT" or node=="RIGHT":
			Global.Grid.eliminate_row(all_mon_spawn_locations2[node][loc].x)
		saved_rows.insert(0, all_mon_spawn_locations2[node][loc].y)
		saved_columns.insert(0, all_mon_spawn_locations2[node][loc].x)

func generate_speeds(list, saved_spd):
	var index = 0
	for i in list:
		var randSpeedGenre = randi()%2
		if randSpeedGenre==0:
			saved_spd.insert(saved_spd.size(), rand_range(green_min_speed, green_max_speed))
		elif randSpeedGenre==1:
			saved_spd.insert(saved_spd.size(), rand_range(red_min_speed, red_max_speed))

func spawn_monsters(list,spds, node_name, rot, velocity):
	var index = 0
	for i in list:
		yield(get_tree().create_timer(0.02), "timeout")
		var mon = monster.instance()
		add_child(mon)
		mon.position = all_mon_spawn_locations2[node_name][i]
		if node_name=="LEFT":
			mon.position.x-=80
		elif node_name=="RIGHT":
			mon.position.x+=80
		elif node_name=="UP":
			mon.position.y-=80
		elif node_name=="DOWN":
			mon.position.y+=80
		mon.rotation = rot
		mon.velocity = velocity
		mon.speed = spds[index]
		index += 1
		yield(get_tree().create_timer(0.02), "timeout")

#can make hints colored regarding of the speed
func spawn_hints(list, spds, node_name):
	var index = 0
	for i in list:
		yield(get_tree().create_timer(0.02), "timeout")
		var hnt = hint.instance()
		add_child(hnt)
		hnt.position = all_mon_spawn_locations2[node_name][i]
		if node_name=="UP":
			hnt.position.y -= 35
		elif node_name=="DOWN":
			hnt.position.y += 15
		if (spds[index]>=green_min_speed&&spds[index]<green_max_speed):
			hnt.animation="green"
		else:
			hnt.animation="red"
		hnt.side = node_name
		hnt.speed = spds[index] * 2.94
		index += 1
		yield(get_tree().create_timer(0.02), "timeout")

func launch_monsters():
	get_tree().call_group("enemies", "call_deferred", "launch")

func launch_hints():
	get_tree().call_group("hint", "call_deferred", "launch")

func animate_hints():
	get_tree().set_group("hint", "playing", true)

func increase_difficulty():
	var i
#	var upOrDown = randi()%2
#	if not upOrDown:
#		i = randi()%2
#		if numbers[i] < 3:
#			numbers[i] += 1
#	else:
#		i = randi()%2 + 2
#		if numbers[i] < 3:
#			numbers[i] += 1
#	if !numbers.has(0):
#		var randz = randi()%4
#		numbers[randz]=0
